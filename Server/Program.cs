using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Server
{

    public class UdpServer
    {
        static IPAddress ip;
        static int count = 0;

        class UdpState
        {
            public UdpClient u;
            public IPEndPoint e;
        }

        public static void receiveCallback(IAsyncResult ar)
        {
            UdpClient u = (UdpClient)((UdpState)(ar.AsyncState)).u;
            IPEndPoint e = (IPEndPoint)((UdpState)(ar.AsyncState)).e;

            Byte[] receiveBytes = u.EndReceive(ar, ref e);

            using (Stream file = File.Open("teste", FileMode.Append))
            {
                file.Write(receiveBytes, 0, receiveBytes.Length);
                count++;
                //Console.WriteLine("Recebidos: " + count );
            }
            

            UdpState s = new UdpState();
            s.e = e;
            s.u = u;
            u.BeginReceive(new AsyncCallback(receiveCallback), s);
        }

        public static void receiveMessages()
        {
            // Receive a message and write it to the console.
            byte[] adress = { 127, 0, 0, 1 };
            ip = new IPAddress(adress);
            IPEndPoint e = new IPEndPoint(ip, 8090);
            UdpClient u = new UdpClient(e);

            UdpState s = new UdpState();
            s.e = e;
            s.u = u;
            u.BeginReceive(new AsyncCallback(receiveCallback), s);
        }

        static void Main(string[] args)
        {
            UdpServer.receiveMessages();

            while (true)
            { }
            //Console.ReadKey();
        }
    }
}
