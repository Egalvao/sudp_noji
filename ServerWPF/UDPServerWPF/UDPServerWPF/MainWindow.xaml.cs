﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System;
using System.Windows.Forms;
using System.IO;
using System.Net;
using System.Net.Sockets;




namespace UDPServerWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        static String PathToSave;
        static IPAddress ip;
        static int count = 0;
        static int Cname = 0;
        static string fileName;

        public MainWindow()
        {
            InitializeComponent();
            LockScreen();
        }

        class UdpState
        {
            public UdpClient u;
            public IPEndPoint e;

        }

        private void btn_SearchFolder_Click(object sender, RoutedEventArgs e) 
        {
            var dialog = new System.Windows.Forms.FolderBrowserDialog();
            System.Windows.Forms.DialogResult result = dialog.ShowDialog();
            txt_PathServer.Text = dialog.SelectedPath;
            PathToSave = dialog.SelectedPath;
           

        }

        private void btn_turnON_Click(object sender, RoutedEventArgs e)
        {
            UnLockScreen();
            InformationServer();

            MainWindow.receiveMessages();
            while (true)
            { }

        }

        private void btn_turnOFF_Click(object sender, RoutedEventArgs e)
        {

        }

        public void LockScreen() {
            txt_PathServer.IsEnabled = false;
            btn_turnOFF.IsEnabled = false;
            txt_PrintValues.IsEnabled = false;

        }
        public void UnLockScreen()
        {
            txt_PathServer.IsEnabled = false;
            btn_turnOFF.IsEnabled = true;
            btn_turnON.IsEnabled = false;
            txt_PrintValues.IsEnabled = false;
            btn_SearchFolder.IsEnabled = false;

        }

        public void InformationServer() {

            txt_myStatus.Text = "No Ar";
            txt_myPort.Text = "27015";
        }


        public static void receiveMessages()
        {
            // Receive a message and write it to the console.
            byte[] adress = { 127, 0, 0, 1 };
            ip = new IPAddress(adress);
            IPEndPoint e = new IPEndPoint(ip, 8090);
            UdpClient u = new UdpClient(e);

            UdpState s = new UdpState();
            s.e = e;
            s.u = u;
            u.BeginReceive(new AsyncCallback(receiveCallback), s);
        }

        public static void receiveCallback(IAsyncResult ar)
        {
            UdpClient u = (UdpClient)((UdpState)(ar.AsyncState)).u;
            IPEndPoint e = (IPEndPoint)((UdpState)(ar.AsyncState)).e;

            Byte[] receiveBytes = u.EndReceive(ar, ref e);

            //Monta Arquivo
            using (Stream file = File.Open("teste", FileMode.Append))
            {
                file.Write(receiveBytes, 0, receiveBytes.Length);
                count++;
            }

            UdpState s = new UdpState();
            s.e = e;
            s.u = u;
            u.BeginReceive(new AsyncCallback(receiveCallback), s);
        }

    }
}
